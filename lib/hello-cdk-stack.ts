import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class HelloCdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    //create an S3 bucket with versioning enabled and encryption

    const bucket = new s3.Bucket(this, 'miniproject3-9492698', {
      bucketName: 'miniproject3-9492698',
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED,
    });

  }
}
