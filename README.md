
[![pipeline status](https://gitlab.oit.duke.edu/xc218/ids721-week-3-project/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/xc218/ids721-week-3-project/-/commits/main)
# IDS721 Week3 Project

## Getting Started
1. Install AWS Tookit in Visual Studio Code.
2. Log in with AWS Builder ID.
3. Install nodejs
4. Install aws-cdk
5. Install bootstrap
6. USe CodeWhisper to generate code

![image](img/code.png)
7. Follow the instruction on [Your first AWS CDK](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html)
8. Deploy

Below is the screenshot of created S3 Bucket
![image](img/version.png)
![image](img/encryption.png)
